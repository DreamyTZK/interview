---
title: 前端面试之MVVM解析
cover: false
mathjax: false
comments: false
categories: 前端面试
abbrlink: 22585d29
date: 2021-02-22 21:05:00
---



# 前端面试之MVVM解析

![img](https://file.acs.pw/picGo/2021/2/22/730bc6358f2fe6c69a1067f366ec51d2.png)

## 一、说一下使用 jquery 和使用框架的区别

### 1.1 jQuery 实现 todo-list

![img](https://file.acs.pw/picGo/2021/2/22/6419f1719a6426e95fd25f8509f8d25a.png)

### 1.2 vue 实现 todo-list

![img](https://file.acs.pw/picGo/2021/2/22/413de7c474015b5840b371a14dce2dd9.png)

### 1.3 jQuery 和框架的区别

- 数据和视图的分离，解耦（开放封闭原则）
- 以数据驱动视图，只关心数据变化，DOM 操作被封装

## 二、说一下对 MVVM 的理解

### 2.1 MVC

- `M - Model` 数据
- `V - View` 视图、界面
- `C - Controller` 控制器、逻辑处理

![img](https://file.acs.pw/picGo/2021/2/22/1159910ea13a44823a00326d5960ede0.png)
![img](https://file.acs.pw/picGo/2021/2/22/42d2d8079daf32576ff8506639b4439a.png)

### 2.2 MVVM

- `Model` - 模型、数据
- `View` - 视图、模板（视图和模型是分离的）
- `ViewModel` - 连接 `Model` 和 `View`

![img](https://file.acs.pw/picGo/2021/2/22/2808dfcba2c24e83aa484aad10ebffb3.png)

### 2.3 关于 ViewModel

- `MVVM` 不算是一种创新
- 但其中的 `ViewModel` 确实一种创新
- 真正结合前端场景应用的创建

![img](https://file.acs.pw/picGo/2021/2/22/afea864dd317228b553ca1b83533fcac.png)
![img](https://file.acs.pw/picGo/2021/2/22/57c5dfe623e0bcc56763f94c79c1f258.png)

### 2.4 MVVM 框架的三大要素

- 响应式：`vue`如何监听到 `data` 的每个属性变化？
- 模板引擎：`vue` 的模板如何被解析，指令如何处理？
- 渲染：`vue` 的模板如何被渲染成 `html` ？以及渲染过程

## 三、vue 中如何实现响应式

### 3.1 什么是响应式

- 修改 data 属性之后，vue 立刻监听到
- data 属性被代理到 vm 上

![img](https://file.acs.pw/picGo/2021/2/22/241d285a9a94046c9f9aec47c1a51ab1.png)

### 3.2 Object.defineProperty

![img](https://file.acs.pw/picGo/2021/2/22/1819373c42d02f318a5e7bec71a3f9ad.png)

### 3.3 模拟实现

![img](https://file.acs.pw/picGo/2021/2/22/1fa6f79727627e04bf59eeb8795327b8.png)

![img](https://file.acs.pw/picGo/2021/2/22/b1cc1b849bc929dc7265baa61c4a12cc.png)

## 四、vue 中如何解析模板

### 4.1 模板是什么

- 本质：字符串
- 有逻辑，如 `v-if` `v-for` 等
- 与 `html` 格式很像，但有很大区别
- 最终还要转换为 `html` 来显示

**模板最终必须转换成 JS 代码，因为**

- 有逻辑（`v-if` `v-for`），必须用 `JS`才能实现
- 转换为 `html` 渲染页面，必须用 `JS` 才能实现
- 因此，模板最重要转换成一个 `JS` 函数（`render` 函数）

![img](https://file.acs.pw/picGo/2021/2/22/30b51b730b4d6bf42540a52faf24493d.png)

### 4.2 render 函数

- 模板中所有信息都包含在了 `render` 函数中
- `this` 即`vm`
- `price` 即 `this.price` 即 `vm.price`，即 `data` 中的 `price`
- `_c` 即 `this._c` 即 `vm._c`

![img](https://file.acs.pw/picGo/2021/2/22/2e01564824d03fc52a224132044213e5.png)

![img](https://file.acs.pw/picGo/2021/2/22/2d72edb646180d84914f74347f3f99d4.png)

![img](https://file.acs.pw/picGo/2021/2/22/b0dba7cb07911f49371e8b4eef48eb95.png)

### 4.3 render 函数与 vdom

- `vm._c` 其实就相当于 `snabbdom`中的 `h` 函数
- `render` 函数执行之后，返回的是 `vnode`

![img](https://file.acs.pw/picGo/2021/2/22/df25644019de4a32b1fdd41e2b616208.png)

![img](https://file.acs.pw/picGo/2021/2/22/9ad2a2f34f9a43687bea47c89d8a9c91.png)

- `updateComponent`中实现了 `vdom` 的 `patch`
- 页面首次渲染执行 `updateComponent`
- `data` 中每次修改属性，执行`updateComponent`

## 五、vue 的整个实现流程

- 第一步：解析模板成 render 函数
- 第二步：响应式开始监听
- 第三步：首次渲染，显示页面，且绑定依赖
- 第四步：`data` 属性变化，触发 `rerender`

![img](https://file.acs.pw/picGo/2021/2/22/f989b3c708dd55caa6dd89bfe61d48b5.png)

### 5.1 第一步：解析模板成 render 函数

![img](https://file.acs.pw/picGo/2021/2/22/377413cd07a6d3eb5fc6628245e0c5da.png)

![img](https://file.acs.pw/picGo/2021/2/22/0fb995ca7de571ed8a83d1c7518bc94e.png)

![img](https://file.acs.pw/picGo/2021/2/22/54767b292c06cff0cd7f5e6657c6c5c3.png)

![img](https://file.acs.pw/picGo/2021/2/22/859d3c7a0d53947f2e50c3c515b6a0fb.png)

- 模板中的所有信息都被 `render`函数包含
- 模板中用到的 `data` 中的属性，都变成了 `JS` 变量
- 模板中的`v-model` `v-for` `v-on` 都变成了 `JS` 逻辑
- `render` 函数返回 `vnode`

### 5.2 第二步：响应式开始监听

- `Object.defineProperty`
- 将 `data` 的属性代理到 `vm`上

![img](https://file.acs.pw/picGo/2021/2/22/24e142e77fa2a3f8197a8956f1197e28.png)

### 5.3 第三步：首次渲染，显示页面，且绑定依赖

- 初次渲染，执行 `updateComponent`，执行 `vm._render()`
- 执行 `render` 函数，会访问到 `vm.list vm.title`
- 会被响应式的 `get` 方法监听到
- 执行 `updateComponent` ，会走到 `vdom` 的 `patch` 方法
- `patch` 将 `vnode`渲染成 `DOM` ，初次渲染完成

![img](https://file.acs.pw/picGo/2021/2/22/5e3875b6c094053f8109f67a7adb8869.png)
![img](https://file.acs.pw/picGo/2021/2/22/fa739c138000ac86b70ad3af26f355c8.png)

**为何要监听 get ，直接监听 set 不行吗？**

- `data` 中有很多属性，有些被用到，有些可能不被用到
- 被用到的会走到 `get` ，不被用到的不会走到 `get`
- 未走到 `get` 中的属性，`set`的时候我们也无需关心
- 避免不必要的重复渲染

![img](https://file.acs.pw/picGo/2021/2/22/290396cfb1dd914a87c03b26645aa3fb.png)

### 5.4 第四步：data 属性变化

![img](https://file.acs.pw/picGo/2021/2/22/9b57422cbc0c73d844b216ad61c633fc.png)
![img](https://file.acs.pw/picGo/2021/2/22/ad078fab833d803132901cab05f9c939.png)

- 修改属性，被响应式的 `set` 监听到
- `set`中执行 `updateComponent`
- updateComponent 重新执行 `vm._render()`
- 生成的 `vnode` 和 `prevVnode` ，通过 `patch`进行对比
- 渲染到 `html` 中

![img](https://file.acs.pw/picGo/2021/2/22/b6a66ee463961398c1786162c3461175.png)