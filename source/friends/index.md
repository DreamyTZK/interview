---
title: 友情链接
date: 2020-02-02 10:00:00
type: "link"
top_img: https://tva1.sinaimg.cn/large/832afe33ly1gbi1rf2bppj21hc0jgwmw.jpg
aside: false
description: 快点和小康交朋友吧！
---
<script src='https://cdn.jsdelivr.net/npm/butterfly-friend@1.0.5/dist/friend.min.js'></script>

<script>
  if(typeof(Friend)=='undefined'){
    location.href='/friends'
  }
  console.log(
    '\n %c butterfly-friend' +
    ' %c https://www.yuque.com/kdoc/bf/friend \n',
    'color: #fff; background: #4285f4; padding:5px 0;',
    'background: #66CCFF; padding:5px 0;'
  )
  var obj = {
    el: '#friend1',
    owner: 'antmoe',
    repo: 'friend',
    direction_sort: 'asc',
    sort_container: ['卡片专属', '乐特专属', '同校PY', '大佬们'],
    labelDescr: {
      大佬们: "<span style='color:red;'>这是一群大佬哦！</span>",
      菜鸡们: "<span style='color:red;'>这是一群菜鸡哦！</span>",
      同校PY: "<span style='color:red;'>同校好友</span>",
      卡片专属: "<span style='color:red;'>大佬专属</span>"
    }
  }
  try {
    btf.isJqueryLoad(function () {
      $('.flink').prepend("<div id='friend1'></div>")
      new Friend(obj)
    })
  } catch (error) {
    window.onload = function () {
      btf.isJqueryLoad(function () {
        $('.flink').prepend("<div id='friend1'></div>")
        new Friend(obj)
      })
    }
  }</script>



## 申请友链格式

{% tabs tab-id %}

<!-- tab 申请友链 -->

1. 进入[friend](https://gitee.com/antmoe/friend)仓库，并且登陆账号
2. 阅读 README 文件
3. 点击 Issue，按照要求进行新建 Issue
4. 等待管理员审核

> 1. 关于修改信息
>
>    如果你已经审核通过，但是需要更改当时的信息，那么你可以再次进入此仓库搜索你的域名即可再次编辑当时发表的 Issue，这样就可以修改你的信息了！
>
>    如果你是在 2020 年 8 月 15 日之前申请的友链需要修改信息需要重新到 gitee 进行新建 issue，等待管理员审核通过后即可修改完成。
>
> 2. 关于清理友链
>
>    我会不定期访问你的友链，如果出现网站无法访问、404、友链入口难以发现、删除本站友链等情况我会直接将你的网站在此站上移除，如需再次添加友链，请重新申请。

<!-- endtab -->

<!-- tab 友联须知 -->

<div class="checkbox cyan checked"><input type="checkbox" checked><p>大佬可不受以下要求限制</p></div>
<div class="checkbox green checked"><input type="checkbox" checked><p>小站是 🍤学习 or 🍥博客站</p></div>

<div class="checkbox green checked"><input type="checkbox" checked><p>至少一篇原创，没有原创的内容📔📕📗📘📙📓的话很快就会死掉的呢  </p></div>

<div class="checkbox minus yellow checked"><input type="checkbox" checked><p>申请友联请务必能在你的友联处看到本站链接。</p></div>

<div class="checkbox minus yellow checked"><input type="checkbox" checked><p>网站要有维护，定期或不定期均可</p></div>

<div class="checkbox minus yellow checked"><input type="checkbox" checked><p>如果内容含有广告会酌情处理。</p></div>

<div class="checkbox times red checked"><input type="checkbox" checked><p>含有违反中国法律内容的一律不通过。</p></div>

<div class="checkbox times red checked"><input type="checkbox" checked><p>网站被QQ、微信标红(不包含非官方提示)的一律不通过。</p></div>

<div class="checkbox times red checked"><input type="checkbox" checked><p>3个月以上没有新文章、网站打不开、取消本站链接本站将直接移除你的链接。</p></div>

<div class="checkbox times red checked"><input type="checkbox" checked><p>新站请过段时间再来申请。</p></div>

<!-- endtab -->

{% endtabs %}

<div class="note danger icon">
    <p>注意：添加友链请到<a href='https://gitee.com/antmoe/friend' target="_blank">friend</a>参照README文件进行添加。在此留言进行申请友链直接忽略！！！</p>
</div>
